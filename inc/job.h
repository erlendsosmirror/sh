/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef JOB_H
#define JOB_H

/*
 * https://www.gnu.org/software/libc/manual/html_node/Data-Structures.html#Data-Structures
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <signal.h>
#include <termios.h>	// For setting stdin behaviour
#include <stdlib.h>		// For exit
#include <spawn.h>		// posix_spawn
#include <sys/wait.h>	// waitpid
#include <dirent.h>		// For checking directories
#include <unistd.h>		// For getcwd

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct Process
{
	struct Process *next;	// next process in pipeline
	pid_t pid;				// process ID
	char completed;			// true if process has completed
	char stopped;			// true if process has stopped
	int status;				// reported status value
}Process;

typedef struct Job
{
	/*struct termios tmodes;		// saved terminal modes
	int stdin, stdout, stderr;	// standard i/o channels*/
	struct Job *nextjob;		// Next valid job
	int pgid;					// Process group id
	int jobid;					// Job id
	Process *firstprocess;		// First process in the job
	char command[64];			// Command line
	int notified;				// True when user has been notified
}Job;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void JobPutInForeground (Job *job, int sendsigcont);
void JobPutInBackground (Job *job, int sendsigcont);
void JobWait (Job *job);
void JobUpdate ();
int JobUpdateProcessStatus (int pid, int status);
int JobIsStopped (Job *job);
int JobIsCompleted (Job *job);
void JobPrintInfo (Job *job, char *statusstring);
void JobNotifyAndClean (int printall);
void JobContinue (Job *job, int foreground);

#endif
