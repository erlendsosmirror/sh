/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <simpleprint/simpleprint.h>
#include "print.h"
#include "command.h"

///////////////////////////////////////////////////////////////////////////////
// Helper functions
///////////////////////////////////////////////////////////////////////////////

/*
 * String compare that returns the number of matching characters
 */
int nmatch (char *a, char *b)
{
	int i = 0;

	while (*a && *b)
	{
		if (*a != *b)
			return i;

		a++;
		b++;
		i++;
	}

	return i;
}

/*
 * Append a slash if the src is a directory
 */
void AutocompleteCheckForFolder (char *dst)
{
	char *src = dst;

	int len = strlen (dst);

	for (int pos = len - 1; pos >= 0; pos--)
	{
		if (dst[pos] == ' ')
		{
			src = &dst[pos+1];
			break;
		}
	}

	struct stat st;

	int ret = stat (src, &st);

	if (!ret && st.st_mode & S_IFDIR)
		strcat (dst, "/");
}

void AutocompletePathSplit (char *dstpath, char *dstpart, char *src)
{
	int len = strlen (src);

	for (int pos = len - 1; pos >= 0; pos--)
	{
		if (src[pos] == '/')
		{
			memcpy (dstpath, src, pos);
			dstpath[pos] = 0;
			strcpy (dstpart, &src[pos+1]);
			return;
		}
	}

	dstpath[0] = 0;
	strcpy (dstpart, src);
}

void AutocompletePrepareInput (char *dstpath, char *dstpart, char *src)
{
	// Copy input
	char input[64];
	char *lastarg = input;
	memcpy (input, src, 64);

	// Check if it ends with a space
	int len = strlen (lastarg);

	if (len && lastarg[len-1] != ' ')
	{
		// Tokenize and get last argument
		CommandArgumentSetup (input);

		for (int i = 0; pargs[i]; i++)
			lastarg = pargs[i];
	}
	else
	{
		// Search for anything
		dstpath[0] = 0;
		dstpart[0] = 0;
		return;
	}

	// Split around /
	AutocompletePathSplit (dstpath, dstpart, lastarg);

	// Debug
	/*printf ("\nInput is \"%s\"\n", src);
	printf ("Got path \"%s\"\n", dstpath);
	printf ("Got comp \"%s\"\n", dstpart);*/
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int Autocomplete (char *cwd, char *str)
{
	// Process input
	char dirpath[64];
	char comp[64];

	AutocompletePrepareInput (dirpath, comp, str);

	int complen = strlen (comp);

	// Open directory
	DIR *d = opendir (dirpath[0] ? dirpath : ".");

	// Check for success
	if (d)
	{
		// Directory entry and length of matching string
		struct dirent *dir;
		char bestmatch[64];
		int bm_maxlen = 64;

		// SimplePrint each directory entry
		while ((dir = readdir (d)) != 0)
		{
			// Only consider it if the user input matches
			if (!strncmp (dir->d_name, comp, complen))
			{
				// Debug
				//printf ("Considering %s\n", dir->d_name);

				// If this is the first time, copy it over
				if (bm_maxlen == 64)
					strcpy (bestmatch, dir->d_name);

				// Get number of matching characters
				int n = nmatch (bestmatch, dir->d_name);

				// If we have fewer matching characters, we need to remove some
				if (n < bm_maxlen)
					bm_maxlen = n;

				bestmatch[n] = 0;
			}
		}

		// Get length of old data
		int oldlen = strlen (str);

		// If we have something to add, add and print it
		if (bm_maxlen < 64 && bm_maxlen > complen)
		{
			strcpy (str + oldlen, bestmatch + complen);
			AutocompleteCheckForFolder (str);
			SimplePrint (str + oldlen);
		}

		// Close directory
		closedir (d);
	}

	// Return the new end position
	return strlen (str);
}

int AutocompleteDoubleTap (char *cwd, char *str)
{
	// Start at new line
	SimplePrint ("\n");

	// Process input
	char dirpath[64];
	char comp[64];

	AutocompletePrepareInput (dirpath, comp, str);

	int complen = strlen (comp);

	// Open directory
	DIR *d = opendir (dirpath[0] ? dirpath : ".");

	// Check for success
	if (d)
	{
		// Directory entry
		struct dirent *dir;

		// SimplePrint each directory entry
		while ((dir = readdir (d)) != 0)
		{
			// SimplePrint it if the start matches
			if (!strncmp (dir->d_name, comp, complen))
			{
				SimplePrint (dir->d_name);
				SimplePrint ("\n");
			}
		}

		// Close directory
		closedir (d);
	}

	return 1;
}
