/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Important notice:
 * Because the posix spawn API lacks support for setting terminal foreground
 * process group (tcsetpgrp), this shell is subject to race coditions.
 * For more information, check the following links:
 *
 * https://github.com/fish-shell/fish-shell/issues/3149
 * http://www.mail-archive.com/ast-developers@research.att.com/msg00719.html
 *
 * Useful links:
 * https://www.gnu.org/software/libc/manual/html_node/Data-Structures.html#Data-Structures
 * https://www.cs.purdue.edu/homes/grr/SystemsProgrammingBook/Book/Chapter5-WritingYourOwnShell.pdf
 * https://github.com/jmreyes/simple-c-shell/blob/master/simple-c-shell.c
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>		// For getcwd
#include <simpleprint/simpleprint.h>

#include "print.h"
#include "term.h"
#include "environ.h"
#include "list.h"
#include "history.h"
#include "prompt.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
/*
 * This is not actually used, as the terminal will send them to
 * the appropriate group for us.
 */
void handler (int sig)
{
	if (sig == SIGINT)
	{
		SimplePrint ("Ctrl^C\n");
		PromptReset ();

		if (firstjob && tcgetpgrp(STDIN_FILENO) == firstjob->pgid)
		{
			SimplePrint ("Send sigint\n");
			kill (-firstjob->pgid, SIGINT);
		}
	}
	else if (sig == SIGTSTP)
	{
		SimplePrint ("Ctrl^Z\n");

		if (firstjob && tcgetpgrp(STDIN_FILENO) == firstjob->pgid)
		{
			SimplePrint ("Send sigtstp\n");
			kill (-firstjob->pgid, SIGTSTP);
		}
	}
}

/*
 * Init shell. Have a look at
 * https://www.gnu.org/software/libc/manual/html_node/Initializing-the-Shell.html
 * https://github.com/jmreyes/simple-c-shell/blob/master/simple-c-shell.c
 */
void InitShell ()
{
	// If the shell is interactive
	if (isatty (STDIN_FILENO) > 0)
	{
		// My pid
		int mypgid;

		// Loop until we are in the foreground
		while (tcgetpgrp (STDIN_FILENO) != (mypgid = getpgrp()))
			kill (-mypgid, SIGTTIN);

		// Set signal handlers
		signal (SIGINT, &handler);
		signal (SIGQUIT, SIG_IGN);
		signal (SIGTSTP, &handler);
		signal (SIGTTIN, SIG_IGN);
		signal (SIGTTOU, SIG_IGN);
		signal (SIGCHLD, &handler);

		// Put ourselves in our own process group
		mypgid = getpid ();

		if (setpgid (mypgid, mypgid) < 0)
		{
			SimplePrint ("Could not get own process group\n");
			TermExit (1);
		}

		// Grab control of the terminal
		tcsetpgrp (STDIN_FILENO, mypgid);
	}
}

int main (int argc, char **argv)
{
	// Process environment variables
	ProcessEnviron ();

	// Reset pid list
	ListClear ();

	// Register the signal
	InitShell ();

	// Get current directory
	if (!getcwd (terminal_currentdir, sizeof(terminal_currentdir)))
	{
		terminal_currentdir[0] = '/';
		terminal_currentdir[1] = 0;
	}

	// Set terminal stuff
	TermSetup ();

	// Clear history
	HistoryInit ();

	// Got to the main loop
	PromptProcess ();

	// Done
	TermExit (0);
	return 0;
}
