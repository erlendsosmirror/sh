/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <simpleprint/simpleprint.h>

#include "print.h"
#include "term.h"		// For TermExit
#include "environ.h"
#include "list.h"
#include "prompt.h"		// For current directory string
#include "command.h"

///////////////////////////////////////////////////////////////////////////////
// Constant data
///////////////////////////////////////////////////////////////////////////////
const char clearscreen[] = {27, 91, '2', 'J', 27, 91, 'H', 0};

///////////////////////////////////////////////////////////////////////////////
// Private data
///////////////////////////////////////////////////////////////////////////////

// Arguments to be used for the process
char *pargs[MAX_ARGS];

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

/*
 * Finds the full path to the supplied program name. It searches in the current
 * directory if ./ is at the start, otherwise it searches the path environment.
 */
char *CommandFindProcessImage (char *name)
{
	// Name and path to the process image we found
	static char processimagename[64];

	// Get the length of name
	int len = strlen (name);

	// Only try current directory if name starts with ./
	if (len > 2 && name[0] == '.' && name[1] == '/')
		return name;

	// Not using ./, check path variables
	for (int i = 0; i < npaths; i++)
	{
		// Open path directory
		DIR *d = opendir (paths[i]);

		if (!d)
		{
			// Opendir error, continue to next directory
			//PrintError ("sh", paths[i]);
			continue;
		}

		// Loop through entries
		struct dirent *dir;

		while ((dir = readdir (d)) != 0)
		{
			// Compare name of entry
			if (!strcmp (name, dir->d_name))
			{
				// Found matching name (use it without checking for execute flag)
				strcpy (processimagename, paths[i]);
				strcat (processimagename, "/");
				strcat (processimagename, name);
				closedir (d);
				return processimagename;
			}
		}

		// Done with this directory
		closedir (d);
	}

	// Could not find matching name
	return 0;
}

/*
 * This is a variant of strtok that is aware of quote marks
 */
char *CommandTok (char *command)
{
	static char *str;

	if (command)
		str = command;

	// Skip all spaces
	while (*str == ' ')
		str++;

	// If there is nothing, return 0
	if (!*str)
		return 0;

	// Start, current and end of string
	char *start = str;
	char *cur = str;
	char *end = str;

	// Find end of string
	while (*end)
	{
		// If we found a space
		if (*end == ' ')
		{
			// Zero it, set str to the next char for next iteration
			*end = 0;
			str = end + 1;

			// Return start of string
			return start;
		}
		else if (*end == '\"')
		{
			// We found a quote mark, need to find the next quote mark
			// Start at the next character
			char *endend = end + 1;

			// Loop until we find another one, while moving data
			while (*endend && *endend != '\"')
			{
				*cur = *endend;
				cur++;
				endend++;
			}

			// If endend is null, the other quote mark was not found, which
			// is an error. Bash makes a multiline prompt in this case, but
			// we just act as if there was a quote mark at the end of the
			// string.

			// If we found a quote, end should point to the next char.
			// Otherwise, it should point to the null byte.
			end = (*endend) ? endend + 1 : endend;

			// Remove the quote mark and set the current position to it.
			// If there is more data, it will be overwritten, otherwise
			// it will function as the terminator.
			*endend = 0;
			*cur = *endend;
		}
		else
		{
			*cur = *end;
			cur++;
			end++;
		}
	}

	// We looped to the end without finding a space
	str = end;
	return start;
}

/*
 * Tokenize the command into pargs. Returns true if the process should
 * be started in the background, zero otherwise.
 */
int CommandArgumentSetup (char *command)
{
	// Start with the first argument
	int pos = 0;

	// Tokenize command on the space char
	do
	{
		// Tokenize
		pargs[pos] = CommandTok (command);

		// If it was zero, we're done
		if (!pargs[pos])
			break;

		// Go to the next position and set it to zero
		pos++;
		pargs[pos] = 0;

		// Make sure strtok is called with a zero pointer next time
		command = 0;
	} while (pos < MAX_ARGS-1);

	// Display arguments for debug
#if 0
	for (int i = 0; i < pos; i++)
	{
		SimplePrint ("Arg ");
		SimplePrint (pargs[i]);
		SimplePrint ("\n");
	}

	return 0;
#endif

	// Check for background process
	if (pos && !strcmp (pargs[pos-1], "&"))
	{
		// Remove the ampersand from the command and return true
		pargs[pos-1] = 0;
		return 1;
	}

	// Do not start in background
	return 0;
}

/*
 * Check for built-in commands, returning true if it was taken.
 */
int CommandCheckBuiltIn (char **str)
{
	if (!strcmp (str[0], "cd"))
	{
		// Try changing directory
		if (chdir (str[1]) < 0)
		{
			PrintError ("sh", str[1]);
			return 1;
		}

		// Get the processed path
		if (!getcwd (terminal_currentdir, sizeof(terminal_currentdir)))
			PrintError ("sh", str[1]);
	}
	else if (!strcmp (str[0], "exit"))
	{
		SimplePrint ("Exiting\n");
		TermExit (0);
	}
	else if (!strcmp (str[0], "fg"))
	{
		if (firstjob)
		{
			if (!JobIsCompleted (firstjob))
				JobPutInForeground (firstjob, JobIsStopped (firstjob));
		}
		else
			SimplePrint ("No such job\n");
	}
	else if (!strcmp (str[0], "bg"))
	{
		if (firstjob)
		{
			if (!JobIsCompleted (firstjob))
				JobPutInBackground (firstjob, JobIsStopped (firstjob));
		}
		else
			SimplePrint ("No such job\n");
	}
	else if (!strcmp (str[0], "jobs"))
		// List background jobs
		JobNotifyAndClean (1);
	else if (!strcmp (str[0], "clear"))
		SimplePrint ((char*)clearscreen);
	else
		// Not a built in command
		return 0;

	// It was a built in command
	return 1;
}

/*
 * Start a process, putting the resulting pid in *pid.
 * The infile and outfile arguments are optional. If supplied,
 * they specify the name of a file that will replace stdin or stdout.
 * The last argument outappend specifies if the output file is opened
 * in append mode or not.
 */
int CommandStartProcess (pid_t *pid, char *infile, char *outfile, int outappend)
{
	// Find executable file with first argument
	char *name = CommandFindProcessImage (pargs[0]);

	// Check if we got anything
	if (!name)
	{
		errno = ENOENT;
		return 1;
	}

	// Spawn attributes object
	posix_spawnattr_t attr;

	// Initialize it
	if (posix_spawnattr_init (&attr))
		SimplePrint ("attr_init failed\n");

	// Set process group
	if (posix_spawnattr_setflags(&attr, POSIX_SPAWN_SETPGROUP))
		SimplePrint ("attr_setflags failed\n");

	if (posix_spawnattr_setpgroup (&attr, 0))
		SimplePrint ("attr_setpgroup failed\n");

	// Prepare for changing input and output file
	posix_spawn_file_actions_t factions;

	if (posix_spawn_file_actions_init (&factions))
		SimplePrint ("factions init failed\n");

	// If input file
	if (infile)
	{
		if (posix_spawn_file_actions_addopen (&factions, 0, infile,
			O_RDONLY, S_IRWXU | S_IRWXG | S_IRWXO))
			SimplePrint ("addopen in failed\n");
	}

	// If output file
	if (outfile)
	{
		int flags = O_WRONLY | O_CREAT;

		flags |= outappend ? O_APPEND : O_TRUNC;

		if (posix_spawn_file_actions_addopen (&factions, 1, outfile, flags,
			S_IRWXU | S_IRWXG | S_IRWXO))
			SimplePrint ("addopen out failed\n");
	}

	// Launch process!
	int ret = posix_spawn (pid, name, &factions, &attr, pargs, environ);

	// Delete attr
	if (posix_spawnattr_destroy (&attr))
		SimplePrint ("attr destroy failed\n");

	// Delete file actions
	if (posix_spawn_file_actions_destroy (&factions))
		SimplePrint ("factions destroy failed\n");

	// Done
	return ret;
}

void CommandProcess (char *str)
{
	// Argument setup, splits str into what will become argv
	int backgroundtask = CommandArgumentSetup (str);

	// Check for built-ins
	if (CommandCheckBuiltIn (pargs))
		return;

	// Get job
	Job *j = ListNewJob ();

	if (!j)
	{
		SimplePrint ("No free slots\n");
		return;
	}

	// Get job id
	int idmin = 0;

	for (Job *job = firstjob; job; job = job->nextjob)
		if (job->jobid > idmin)
			idmin = job->jobid;

	j->jobid = idmin + 1;

	// Add to list
	j->nextjob = firstjob;
	firstjob = j;

	// Alloc first process
	j->firstprocess = ListNewProcess ();

	if (!j->firstprocess)
	{
		SimplePrint ("Could not allocate process\n");
		ListDeleteJobFromActive (j);
		return;
	}

	// Copy name
	strcpy (j->command, pargs[0]);

	// Check redirection
	char *infile = 0;
	char *outfile = 0;
	int outappend = 0;

	for (int i = 0; pargs[i]; i++)
	{
		// Require at least one additional argument
		if (!pargs[i+1])
			break;

		if (!strcmp (pargs[i], "<"))
		{
			pargs[i] = 0;
			infile = pargs[i+1];
		}
		else if (!strcmp (pargs[i], ">"))
		{
			pargs[i] = 0;
			outfile = pargs[i+1];
		}
		else if (!strcmp (pargs[i], ">>"))
		{
			pargs[i] = 0;
			outfile = pargs[i+1];
			outappend = 1;
		}
	}

	// Start it
	int ret = CommandStartProcess (&j->firstprocess->pid, infile, outfile, outappend);

	if (ret != 0)
	{
		PrintError ("sh", pargs[0]);
		ListDeleteJobFromActive (j);
		return;
	}

	// Also set process group in job now
	j->pgid = j->firstprocess->pid;

	// Put in background if set as background
	if (backgroundtask)
		JobPutInBackground (j, 0);
	else
	{
		// Otherwise, put in foreground and wait for it to complete (or
		// be interrupted)
		JobPutInForeground (j, 0);

		// If it exited successfully, we may clean up already without printing crap
		if (JobIsCompleted (j))
			ListDeleteJobFromActive (j);
	}
}
