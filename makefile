###############################################################################
# Project name and files
###############################################################################
PROJECT_NAME = sh
PROJECT_FILES = $(shell find src -name "*.*")

FLASHDIR = /

NOREL_BASE_TEXT = 0x20008000
NOREL_BASE_RODATA = 0x2000B000
NOREL_BASE_DATA = 0x2000C000
NOREL_BASE_BSS = 0x2000C42C

USR_INC = -I../../lib/libuser/inc -I./inc
USR_LIB = -L../../lib/libuser/build
USR_LIBS = -luser

###############################################################################
# Compiler flags, file processing and makefile execution
###############################################################################
include ../../util/build/makefile/flags
include ../../util/build/makefile/processfiles
include ../../util/build/makefile/targets
