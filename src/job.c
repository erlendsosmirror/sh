/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <errno.h>
#include <simpleprint/simpleprint.h>
#include "job.h"
#include "list.h"
#include "print.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void JobPutInForeground (Job *job, int sendsigcont)
{
	// Put the job process group into the foreground
	tcsetpgrp (STDIN_FILENO, job->pgid);

	// Send the SIGCONT signal if specified
	if (sendsigcont)
	{
		// Set the jobs terminal attributes
		//tcsetattr (STDIN_FILENO, TCSADRAIN, &job->tmodes);

		// Send the signal to the group
		if (kill (-job->pgid, SIGCONT) < 0)
			SimplePrint ("Error sending SIGCONT\n");
	}

	// Wait for job
	JobWait (job);

	// Put the sell process group into the foreground
	tcsetpgrp (STDIN_FILENO, getpid ());

	// Restore shell's terminal modes
	// tcgetattr (STDIN_FILENO, &job->tmodes);
	// tcsetattr (STDIN_FILENO, TCSADRAIN, &shelltmodes);
}

void JobPutInBackground (Job *job, int sendsigcont)
{
	if (sendsigcont)
	{
		// Set the group to running
		for (Process *p = job->firstprocess; p; p = p->next)
		{
			p->status = 0;
			p->stopped = 0;
			p->completed = 0;
		}

		// Send signal
		if (kill (-job->pgid, SIGCONT) < 0)
			SimplePrint ("Error sending SIGCONT\n");
	}
}

void JobWait (Job *job)
{
	// Local vars
	int status;
	int pid;

	// Update process statuses while waiting for either job to stop or complete
	do
		pid = waitpid (-1, &status, WUNTRACED);
	while (!JobUpdateProcessStatus (pid, status)
		&& !JobIsStopped (job)
		&& !JobIsCompleted (job));
}

void JobUpdate ()
{
	// Local vars
	int status;
	int pid;

	// Just update stuff until there is nothing new
	do
		pid = waitpid (-1, &status, WUNTRACED | WNOHANG);
	while (!JobUpdateProcessStatus (pid, status));
}

int JobUpdateProcessStatus (int pid, int status)
{
	// Local vars
	Job *job;
	Process *process;

	// If pid seems valid
	if (pid > 0)
	{
		// Loop through jobs and processes
		for (job = firstjob; job; job = job->nextjob)
		{
			for (process = job->firstprocess; process; process = process->next)
			{
				// If this is the process, set status and update flags
				if (process->pid == pid)
				{
					process->status = status;

					if (WIFSTOPPED(status))
						process->stopped = 1;
					else
					{
						process->completed = 1;

						// If it was terminated by a signal, print
						//if (WIFSIGNALED(status))
						//	printf ("pid %i terminated by signal %i\n", pid, WTERMSIG(status));
					}

					//printf ("Update pid %i state %i\n", pid, status);

					return 0;
				}
			}
		}

		// If we reached this point, it means we could not find the pid in the list
		SimplePrint ("Could not find pid in job list\n");
		return -1;
	}
	else if (pid == 0 || errno == ECHILD)
	{
		// If we're trying to update our list using a non-blocking waitpid and
		// nothing new happened, we are probably ending up here
		return -1;
	}
	else
	{
		// Pid was less than zero
		SimplePrint ("JobUpdateProcessStatus error\n");
		return -1;
	}
}

int JobIsStopped (Job *job)
{
	for (Process *p = job->firstprocess; p; p = p->next)
		if (!p->completed && !p->stopped)
			return 0;
	return 1;
}

int JobIsCompleted (Job *job)
{
	for (Process *p = job->firstprocess; p; p = p->next)
		if (!p->completed)
			return 0;
	return 1;
}

void JobPrintInfo (Job *job, char *statusstring)
{
	//printf ("[%i] (%i) %s %s\n", job->jobid, job->pgid, statusstring, job->command);
	char str[16];
	_itoa (job->jobid, str);
	SimplePrint ("[");
	SimplePrint (str);
	SimplePrint ("] (");
	_itoa (job->pgid, str);
	SimplePrint (str);
	SimplePrint (") ");
	SimplePrint (statusstring);
	SimplePrint (" ");
	SimplePrint (job->command);
	SimplePrint ("\n");
}

void JobNotifyAndClean (int printall)
{
	// Update information first, non-blocking
	JobUpdate ();

	// Local vars
	Job *jnext = 0;

	// Loop through active jobs
	for (Job *job = firstjob; job; job = jnext)
	{
		// Set next
		jnext = job->nextjob;

		// Check
		if (JobIsCompleted (job))
		{
			// Notify and delete completed jobs
			JobPrintInfo (job, "Done");
			ListDeleteJobFromActive (job);
		}
		else if (JobIsStopped (job) && (printall || !job->notified))
		{
			JobPrintInfo (job, "Stopped");
			job->notified = 1;
		}
		else if (printall)
			JobPrintInfo (job, "Running");
	}
}

void JobContinue (Job *job, int foreground)
{
	// Mark all processes as running
	for (Process *p = job->firstprocess; p; p = p->next)
		p->stopped = 0;

	job->notified = 0;

	// Continue the job
	if (foreground)
		JobPutInForeground (job, 1);
	else
		JobPutInBackground (job, 1);
}
