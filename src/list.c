/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "list.h"

///////////////////////////////////////////////////////////////////////////////
// Private data
///////////////////////////////////////////////////////////////////////////////

// List of jobs visible to other parts of the program, exported in list.h
Job *firstjob = 0;

// Internal list of free job objects
Job joblist[MAX_JOBLIST];
Job *joblistfirst;

// Internal list of free process objects
Process processlist[MAX_PROCESSLIST];
Process *processlistfirst;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void ListClear (void)
{
	// Job list
	joblistfirst = &joblist[0];

	Job *j = joblistfirst;

	for (int i = 1; i < MAX_JOBLIST; i++)
	{
		joblist[i].nextjob = 0;
		j->nextjob = &joblist[i];
		j = j->nextjob;
	}

	// Process list
	processlistfirst = &processlist[0];

	Process *p = processlistfirst;

	for (int i = 1; i < MAX_PROCESSLIST; i++)
	{
		processlist[i].next = 0;
		p->next = &processlist[i];
		p = p->next;
	}
}

Job *ListNewJob ()
{
	Job *j = joblistfirst; //malloc (sizeof (Job));

	if (j)
	{
		// Get new head
		joblistfirst = j->nextjob;

		// Erase job
		j->nextjob = 0;
		j->pgid = 0;
		j->jobid = 0;
		j->firstprocess = 0;
		j->command[0] = 0;
		j->notified = 0;
	}

	return j;
}

void ListDeleteJob (Job *job)
{
	for (Process *p = job->firstprocess; p; )
	{
		Process *nextp = p->next;
		ListDeleteProcess (p);
		p = nextp;
	}

	job->nextjob = joblistfirst;
	joblistfirst = job;
}

void ListDeleteJobFromActive (Job *j)
{
	Job *jnext = 0;
	Job *jprev = 0;

	for (Job *job = firstjob; job; job = jnext)
	{
		// Set next
		jnext = job->nextjob;

		// Check
		if (job == j)
		{
			if (jprev)
				jprev->nextjob = jnext;
			else
				firstjob = jnext;

			ListDeleteJob (job);
			break;
		}

		jprev = job;
	}
}

Process *ListNewProcess (void)
{
	Process *p = processlistfirst;

	if (p)
	{
		// Get new head
		processlistfirst = p->next;

		// Erase process
		p->next = 0;
		p->pid = 0;
		p->completed = 0;
		p->stopped = 0;
		p->status = 0;
	}

	return p;
}

void ListDeleteProcess (Process *process)
{
	process->next = processlistfirst;
	processlistfirst = process;
}
