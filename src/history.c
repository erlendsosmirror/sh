/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include "history.h"

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct history_data
{
	char text[64*16];		// 1KiB
	int firstline;			// Pointer to start of oldest line
	int lastline;			// Pointer to start of most recent line
	int curpos;				// Pointer to start of currently displayed line
	unsigned int nbytes;	// Number of unused bytes
	char dirty;				// True if the currently displayed line is dirtied
	char nobackwards;
	char changed;
}history_data;

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
history_data hdat;

///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////

/*
 * Erase the first (oldest) entry in the history. After this operation the
 * first line index will point to the now oldest entry. If the current
 * position was the first entry, it is also moved. This function has no
 * effect if the first line is empty.
 */
void HistoryEraseFirst (void)
{
	// Get the position of the first line and initialize count
	int cur = hdat.firstline;
	int count = 0;

	// If there is anything in the first line (history is not empty)
	if (hdat.text[cur])
	{
		// Erase the data
		while (hdat.text[cur])
		{
			count++;
			hdat.text[cur] = 0;
			cur = (cur + 1) & 1023;
		}

		// Include the null byte in the count and skip over it
		count++;
		cur = (cur + 1) & 1023;
	}

	// Set curpos if it was the first line
	if (hdat.curpos == hdat.firstline)
		hdat.curpos = cur;

	// Set the first line index
	hdat.firstline = cur;

	// Add the count to nbytes
	hdat.nbytes += count;
}

/*
 * Same as EraseFirst, except it does not alter the current position,
 * and it operates on the last line.
 */
void HistoryEraseLast (void)
{
	// Get the position of the last line and initialize count
	int cur = hdat.lastline;
	int count = 0;

	// If there is anything in the last line (history is not empty)
	if (hdat.text[cur])
	{
		// Erase the data
		while (hdat.text[cur])
		{
			count++;
			hdat.text[cur] = 0;
			cur = (cur + 1) & 1023;
		}

		// Include the null byte in the count and skip over it
		count++;
		cur = (cur + 1) & 1023;
	}

	// Add the count to nbytes
	hdat.nbytes += count;
}

/*
 * Erases old entries until we have at least the specified number
 * of bytes free.
 */
void HistoryFree (int bytesneeded)
{
	// Abort if nytesneeded is invalid
	if (bytesneeded >= 1024)
		return;

	// If we're in need of more bytes
	while (bytesneeded > hdat.nbytes)
		HistoryEraseFirst ();
}

/*
 * Replace the newest entry with the specified string
 */
void HistoryChangeLast (char *str, int length)
{
	// Erase last
	HistoryEraseLast ();

	// Check bytes
	HistoryFree (length + 1);

	// Write
	for (int i = 0; i < length; i++)
		hdat.text[(hdat.lastline + i) & 1023] = str[i];

	hdat.text[(hdat.lastline + length) & 1023] = 0;
	hdat.nbytes -= (length + 1);
}

/*
 * Moves the current one step backwards in the history
 */
void HistoryBackwards (void)
{
	// Do not do anything if already at the oldest entry
	if (hdat.curpos == hdat.firstline)
		return;

	// Skip to the null byte
	hdat.curpos = (hdat.curpos - 1) & 1023;

	// Loop until we reach the oldest entry or a null byte
	do
	{
		if (hdat.curpos == hdat.firstline)
			return;

		hdat.curpos = (hdat.curpos - 1) & 1023;
	} while (hdat.text[hdat.curpos]);

	// We have to increment our position if we reached a null byte and
	// the history is not empty
	if (!hdat.text[hdat.curpos] && hdat.curpos != hdat.lastline)
		hdat.curpos = (hdat.curpos + 1) & 1023;
}

/*
 * Moves the current one step forwards in the history
 */
void HistoryForwards (void)
{
	// Loop until we reach a null byte
	do
	{
		// Return if we found the most recent line
		if (hdat.curpos == hdat.lastline)
			return;

		hdat.curpos = (hdat.curpos + 1) & 1023;
	} while (hdat.text[hdat.curpos]);

	// Increment if we reached a null byte and the history is not empty
	if (!hdat.text[hdat.curpos] && hdat.curpos != hdat.lastline)
		hdat.curpos = (hdat.curpos + 1) & 1023;
}

/*
 * Copy the string at the current position into str,
 * and return its length. Any unused space up to index 64
 * is erased.
 */
int HistoryCopy (char *str)
{
	int pos = hdat.curpos;
	int len = 0;

	while (hdat.text[pos])
	{
		str[len] = hdat.text[pos];
		pos = (pos + 1) & 1023;
		len++;
	}

	for (int i = len; i < 64; i++)
		str[i] = 0;

	return len;
}

/*
 * Add a line to the history. Does not change the current position.
 */
int HistoryAdd (char *str, int length)
{
	// If there are not enough bytes, free enough bytes by deleting older lines
	HistoryFree (length + 1);

	// Find the end of lastline
	int pos = hdat.lastline;

	if (hdat.text[pos])
	{
		while (hdat.text[pos])
			pos = (pos + 1) & 1023;

		pos = (pos + 1) & 1023;

		hdat.lastline = pos;
	}

	// Write the line
	for (int i = 0; i < length; i++)
		hdat.text[(pos + i) & 1023] = str[i];

	hdat.text[(pos + length) & 1023] = 0;

	// Decrement nbytes
	hdat.nbytes -= (length + 1);

	// Return position of added line
	return pos;
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void HistoryInit (void)
{
	memset (&hdat, 0, sizeof (history_data));
	hdat.nbytes = sizeof (hdat.text);
	hdat.nobackwards = 1;
}

int HistoryPrevious (char *str, int length)
{
	// If we changed something we have to store it
	if (hdat.changed)
	{
		hdat.changed = 0;
		HistoryChangeLast (str, length);
	}

	// Only skip backwards if we are supposed to
	if (!hdat.nobackwards)
		HistoryBackwards ();

	hdat.nobackwards = 0;

	// Copy the current string
	return HistoryCopy (str);
}

int HistoryNext (char *str, int length)
{
	// If we changed something we have to store it
	if (hdat.changed)
	{
		hdat.changed = 0;
		HistoryChangeLast (str, length);
	}

	// In case the user has not typed anything, provide an empty line
	if (hdat.curpos == hdat.lastline && !hdat.dirty)
	{
		hdat.nobackwards = 1;
		memset (str, 0, 64);
		return 0;
	}

	// Skip forwards, clear flag and copy data
	HistoryForwards ();
	hdat.nobackwards = 0;
	return HistoryCopy (str);
}

int HistoryStore (char *str, int length)
{
	// Update last line
	if (hdat.dirty)
		HistoryChangeLast (str, length);
	else
	{
		// Should only add if the strings are different
		int pos = hdat.lastline;
		int i = 0;

		while (str[i] && (hdat.text[pos] == str[i]))
		{
			pos = (pos + 1) & 1023;
			i++;
		}

		if (hdat.text[pos] != str[i])
			HistoryAdd (str, length);
	}

	// Clear dirty/changed and disable backwards skipping for one time
	hdat.dirty = 0;
	hdat.changed = 0;
	hdat.nobackwards = 1;

	// Set current and add
	hdat.curpos = hdat.lastline;

	// Done
	return 0;
}

void HistoryEdited (char *str, int length)
{
	// If clean state and str is valid
	if (!hdat.dirty && length)
	{
		// Add the line and set last line as dirty
		HistoryAdd (str, length);
		hdat.dirty = 1;
	}

	// When we change a line, set our position in the history to last
	hdat.curpos = hdat.lastline;

	// Disable skipping back for one uparrow key press
	hdat.nobackwards = 0;

	// Set changed flag
	hdat.changed = 1;
}
