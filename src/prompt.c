/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <time.h>	// For nanosleep
#include <simpleprint/simpleprint.h>

#include "print.h"
#include "list.h"
#include "auto.h"
#include "history.h"
#include "command.h"

///////////////////////////////////////////////////////////////////////////////
// Constant data
///////////////////////////////////////////////////////////////////////////////
const char leftarrow[] = {27, 91, 68, 0};
const char rightarrow[] = {27, 91, 67, 0};
const char downarrow[] = {27, 91, 66, 0};
const char uparrow[] = {27, 91, 65, 0};
const char backspace[] = {0x08, ' ', 0x08, 0};

const char setcolora[] = {0x1B, 0x5B, '3', '2', ';', '1', 'm', 0};
const char setcolorb[] = {0x1B, 0x5B, '3', '4', ';', '1', 'm', 0};
const char resetcolor[] = {0x1B, 0x5B, '0', 'm', 0};

///////////////////////////////////////////////////////////////////////////////
// Private data
///////////////////////////////////////////////////////////////////////////////

// Current working directory
char terminal_currentdir[64];

// Current command being typed
char work[64];
int terminal_pos = 0;
int terminal_endpos = 0;

// Used for interpreting escape sequences (like arrow keys)
int inescape = 0;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void PromptResetText ()
{
	SimplePrint ((char*)setcolora);
	SimplePrint ("ejs ");
	SimplePrint ((char*)setcolorb);
	SimplePrint (terminal_currentdir);
	SimplePrint ("\n$ ");
	SimplePrint ((char*)resetcolor);
}

void PromptReset ()
{
	inescape = 0;
	terminal_pos = 0;
	terminal_endpos = 0;
	PromptResetText ();
}

void PromptClearLine (void)
{
	// Erase characters to the right of the cursor with spaces
	for (int i = terminal_pos; i < terminal_endpos; i++)
		PrintChar (' ');

	// Backspace all the way back
	for (int i = 0; i < terminal_endpos; i++)
		SimplePrint ((char*)backspace);
}

void PromptProcessEscape (char recvchar)
{
	if (inescape == 27)
		inescape = recvchar;
	else if (inescape == 91)
	{
		if (recvchar == 65)
		{
			// Uparrow
			PromptClearLine ();
			terminal_endpos = HistoryPrevious (work, terminal_endpos);
			terminal_pos = terminal_endpos;
			SimplePrint (work);
		}
		else if (recvchar == 68 && terminal_pos)
		{
			// Leftarrow
			SimplePrint ((char*)leftarrow);
			terminal_pos--;
		}
		else if (recvchar == 67 && terminal_pos < terminal_endpos)
		{
			// Rightarrow
			SimplePrint ((char*)rightarrow);
			terminal_pos++;
		}
		else if (recvchar == 66)
		{
			// Downarrow
			PromptClearLine ();
			terminal_endpos = HistoryNext (work, terminal_endpos);
			terminal_pos = terminal_endpos;
			SimplePrint (work);
		}
		else if (recvchar == 70)
		{
			// End
			for (; terminal_pos < terminal_endpos; terminal_pos++)
				SimplePrint ((char*)rightarrow);
		}
		else if (recvchar == 72)
		{
			// Home
			for (; terminal_pos; terminal_pos--)
				SimplePrint ((char*)leftarrow);
		}

		inescape = 0;
	}
	else
		inescape = 0;
}

void PromptProcess ()
{
	// Receive character and echo string
	char recvchar;
	char oldchar;
	char echo[2];
	echo[1] = 0;

	// Reset
	PromptReset ();

	// Main loop
	while (1)
	{
		// Get one character
		int ret = read (0, &recvchar, 1);

		// Did we get one?
		if (ret != 1)
		{
			// No, sleep and retry
			struct timespec req;
			req.tv_sec = 0;
			req.tv_nsec = 3 * 1000000;
			nanosleep (&req, 0);

			continue;
		}

		// If we are processing an escape sequence
		if (inescape)
		{
			// Process it and restart loop
			PromptProcessEscape (recvchar);
			continue;
		}

		// Store old character and set echo string
		oldchar = echo[0];
		echo[0] = recvchar;

		// Check character
		if (recvchar == '\r' || recvchar == '\n')
		{
			// Echo the character and a newline
			SimplePrint (echo);
			SimplePrint ("\n");

			// Terminate our work buffer
			work[terminal_endpos] = 0;

			// If the command is non-empty
			if (terminal_endpos)
			{
				// Store it in the history and process it
				HistoryStore (work, terminal_endpos);
				CommandProcess (work);
			}

			// SimplePrint job notifications
			JobNotifyAndClean (0);

			// Reset the terminal
			PromptReset ();
		}
		else if (recvchar == '\t')
		{
			// Terminate our work buffer
			work[terminal_endpos] = 0;

			// Check for double tab
			if (oldchar == '\t')
			{
				// Prevent another double tab by erasing the echo?
				echo[0] = 0;

				// Perform auto complete with list
				if (AutocompleteDoubleTap (terminal_currentdir, work))
				{
					// There were multiple options, so we need to reset the prompt
					PromptResetText ();
					SimplePrint (work);
					terminal_pos = terminal_endpos;
				}
			}
			else
			{
				// Single-tab auto complete
				terminal_endpos = Autocomplete (terminal_currentdir, work);
				terminal_pos = terminal_endpos;
			}

			// Indicate that the current line has changed
			HistoryEdited (work, terminal_endpos);
		}
		else if (recvchar == 0x03)
		{
			// We got a ^C character from standard input. Should normally not
			// happen, but keeping this for debug.
			SimplePrint ("^C from stdin\n");
			PromptReset ();
			HistoryEdited (work, terminal_endpos);
		}
		else if (recvchar == 0x1A)
		{
			// We got a ^Z character from standard input. Should normally not
			// happen, but keeping this for debug.
			SimplePrint ("^Z from stdin\n");
			PromptReset ();
			HistoryEdited (work, terminal_endpos);
		}
		else if (recvchar == 0x08 || recvchar == 0x7F)
		{
			// Backspace at the end or in the middle. Check if there
			// is anything to remove.
			if (terminal_pos)
			{
				// Move cursor one step to the left
				PrintChar (0x08);

				// Re-print the remaining command text, does nothing if
				// the cursor is already at the end.
				for (int i = terminal_pos; i < terminal_endpos; i++)
					PrintChar (work[i]);

				// Erase the last position with a space
				PrintChar (' ');
				PrintChar (0x08);

				// Move data in the work buffer and move the cursor back at the same time
				for (int i = terminal_pos; i < terminal_endpos; i++)
				{
					work[i-1] = work[i];
					PrintChar (0x08);
				}

				// Decrement indexes
				terminal_pos--;
				terminal_endpos--;

				// Indicate changed history
				HistoryEdited (work, terminal_endpos);
			}
		}
		else if (recvchar == 27)
			// Start of escape sequence
			inescape = recvchar;
		else
		{
			// Insert or append a character
			// Increment indexes
			terminal_pos++;
			terminal_endpos++;

			// Check if there is space for another character, if not: reset
			if (terminal_endpos == 64)
				PromptReset ();
			else
			{
				// Move data to the right of the cursor, no effect if
				// the cursor is already at the end.
				for (int i = terminal_endpos-1; i >= terminal_pos; i--)
						work[i] = work[i-1];

				// Insert the new character and echo it
				work[terminal_pos-1] = recvchar;
				SimplePrint (echo);

				// Re-print right hand text
				for (int i = terminal_pos; i < terminal_endpos; i++)
					PrintChar (work[i]);

				// Move the cursor back
				for (int i = terminal_pos; i < terminal_endpos; i++)
					PrintChar (0x08);
			}

			// Indicate changed history
			HistoryEdited (work, terminal_endpos);
		}
	}
}
