/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <simpleprint/simpleprint.h>
#include "environ.h"

///////////////////////////////////////////////////////////////////////////////
// Private data
///////////////////////////////////////////////////////////////////////////////

// Path broken into separate strings
char *paths[MAX_PATHS];
int npaths;

// Copy buffer for the variable, so that we do not break it
char pathcpy[128];

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void DumpPathStrings (void)
{
	for (int i = 0; i < npaths; i++)
	{
		SimplePrint (paths[i]);
		SimplePrint ("\n");
	}
}

void ProcessEnviron (void)
{
	// Reset number of path entries
	memset (paths, 0, sizeof (paths));
	npaths = 0;

	// Search for path
	for (int i = 0; environ[i]; i++)
	{
		// Make copy
		int len = strlen (environ[i]);

		if (len > 127)
		{
			SimplePrint ("Warning: Env truncated\n");
			len = 127;
		}

		memcpy (pathcpy, environ[i], len);
		pathcpy[len] = 0;

		// Tokenize on the = char
		char *tok = strtok (pathcpy, "=");

		// Is it the path environment variable?
		if (strcmp (tok, "PATH"))
			continue;

		// Tokenize the substrings and store them
		for (int j = 0; j < MAX_PATHS; j++)
		{
			paths[j] = strtok (0, ":");

			if (paths[j])
			{
				npaths++;

				if (j == MAX_PATHS-1)
					SimplePrint ("No space for all path parts\n");
			}
			else
				break;
		}

		// Since we found path, we're done. This assumes there is only one path.
		break;
	}
}
